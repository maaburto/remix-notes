import type {
  MetaFunction,
  LinksFunction,
  ErrorBoundaryComponent,
} from "@remix-run/node";
import {
  Link,
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
  useCatch,
} from "@remix-run/react";

import rootStyles from "~/styles/main.css";
import { MainNavigation } from "~/components/MainNavigation";
import type { CatchBoundaryComponent } from "@remix-run/react/dist/routeModules";

export const meta: MetaFunction = () => ({
  charset: "utf-8",
  title: "New Remix App",
  viewport: "width=device-width,initial-scale=1",
});

export const links: LinksFunction = () => [
  {
    rel: "stylesheet",
    href: rootStyles,
  },
];

export const CatchBoundary: CatchBoundaryComponent = () => {
  const caughtResponse = useCatch();
  const message = caughtResponse?.data?.message || "Something went wrong";

  return (
    <html lang='en'>
      <head>
        <Meta />
        <Links />
        <title>{caughtResponse?.statusText}</title>
      </head>
      <body>
        <header>
          <MainNavigation />
        </header>
        <main className='error'>
          <h1>{caughtResponse?.statusText}</h1>
          <p>{message}</p>
          <p>
            Back To Safety! <Link to={"/"}>safety!</Link>
          </p>
        </main>
        <ScrollRestoration />
        <Scripts />
        <LiveReload />
      </body>
    </html>
  );
};

export const ErrorBoundary: ErrorBoundaryComponent = ({ error }) => {
  return (
    <html lang='en'>
      <head>
        <Meta />
        <Links />
        <title>An error occurred!</title>
      </head>
      <body>
        <header>
          <MainNavigation />
        </header>
        <main className='error'>
          <h1>An error occurred!</h1>
          <p>{error.message}</p>
          <p>
            Back To Safety! <Link to={"/"}>safety!</Link>
          </p>
        </main>
        <ScrollRestoration />
        <Scripts />
        <LiveReload />
      </body>
    </html>
  );
};

export default function App() {
  return (
    <html lang='en'>
      <head>
        <Meta />
        <Links />
      </head>
      <body>
        <header>
          <MainNavigation />
        </header>
        <Outlet />
        <ScrollRestoration />
        <Scripts />
        <LiveReload />
      </body>
    </html>
  );
}
