import type {
  LinksFunction,
  LoaderFunction,
  MetaFunction,
} from "@remix-run/node";
import { json } from "@remix-run/node";
import { useLoaderData } from "@remix-run/react";
import {
  NoteDetails,
  noteDetailsLinkDescriptors,
} from "~/components/NoteDetails";
import { getStoredNotes } from "~/data/notes";
import type { Note } from "~/models/note";

export default function NoteDetailsPage() {
  const note = useLoaderData<Note>();

  return (
    <main id='note-details'>
      <NoteDetails id={note.id} content={note.content} title={note.title} />
    </main>
  );
}

export const loader: LoaderFunction = async ({ params }) => {
  const noteId = params.noteId as string;
  const note = (await getStoredNotes()).find((note) => note.id === noteId);

  if (!note || !note?.id) {
    throw json(
      {
        message: "Could not find the note for id " + noteId,
      },
      {
        status: 404,
        statusText: "Not Found",
      }
    );
  }

  return note;
};

export const links: LinksFunction = () => [...noteDetailsLinkDescriptors];

export const meta: MetaFunction<typeof loader> = ({ data }) => {
  const note = data as Note;
  return {
    title: `Manage ${note.title} Note`,
    description: "Manage your notes with ease.",
  };
};
