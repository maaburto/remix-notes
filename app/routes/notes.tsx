import type {
  LinksFunction,
  ActionFunction,
  LoaderFunction,
  ErrorBoundaryComponent,
  MetaFunction,
} from "@remix-run/node";
import { json } from "@remix-run/node";
import { redirect } from "@remix-run/node";
import { Link, useCatch, useLoaderData } from "@remix-run/react";
import type { CatchBoundaryComponent } from "@remix-run/react/dist/routeModules";
import { NewNote, newNoteLinkDescriptors } from "~/components/NewNote";
import {
  noteItemlinksDescriptor,
  noteItemMetalinksDescriptor,
  NoteList,
  noteListlinksDescriptor,
} from "~/components/Notes";
import { getStoredNotes, storeNotes } from "~/data/notes";
import type { INewNote, Note } from "~/models/note";

export default function Notes() {
  const notes = useLoaderData<Note[]>();

  return (
    <main>
      <NewNote />
      <NoteList notes={notes} />
    </main>
  );
}

export const loader: LoaderFunction = async () => {
  const notes = await getStoredNotes();

  // Behind Scences Remix does this! To return an Standard JSON Response but is the same
  // making `return notes`
  // return json(notes);

  if (!notes || notes?.length === 0) {
    throw json(
      {
        message: "Could not find any notes.",
      },
      {
        status: 404,
        statusText: "Not Found",
      }
    );
  }

  return notes;
};

export const action: ActionFunction = async ({ request }) => {
  const formData = await request.formData();

  const noteData = (Object.fromEntries(formData) as unknown as INewNote) || {
    title: "",
    content: "",
  };

  if (noteData.title.trim().length < 5) {
    return {
      message: "Invalid title - must be at least 5 characters long.",
    };
  }

  const existingNotes = await getStoredNotes();
  const newNote: Note = {
    ...noteData,
    id: new Date().toISOString(),
  };
  const updatedNotes = existingNotes.concat(newNote);
  await storeNotes(updatedNotes);

  // Typically should be a response, but you can return a redirect as response too!!
  return redirect("/notes");
};

export const links: LinksFunction = () => [
  ...newNoteLinkDescriptors,
  ...noteListlinksDescriptor,
  ...noteItemlinksDescriptor,
  ...noteItemMetalinksDescriptor,
];

export const meta: MetaFunction = () => ({
  title: "All Notes",
  description: "Manage your notes with ease.",
});

export const CatchBoundary: CatchBoundaryComponent = () => {
  const caughtResponse = useCatch();
  const message = caughtResponse.data?.message || "Data not found.";

  return (
    <main>
      <NewNote />
      <section className='error'>
        <p>{message}</p>
      </section>
    </main>
  );
};

export const ErrorBoundary: ErrorBoundaryComponent = ({ error }) => {
  return (
    <main className='error'>
      <h1>An error related to your notes ocurred!</h1>
      <p>{error.message}</p>
      <p>
        Back To Safety! <Link to={"/"}>safety!</Link>
      </p>
    </main>
  );
};
