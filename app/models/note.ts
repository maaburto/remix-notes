export interface INewNote {
  title: string;
  content: string;
}

export class Note implements INewNote {
  id: string = "";
  title: string = "";
  content: string = "";
}
