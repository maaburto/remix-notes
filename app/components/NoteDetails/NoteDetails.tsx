import type { LinkDescriptor } from "@remix-run/node";
import { Link } from "@remix-run/react";
import type { FC } from "react";
import type { Note } from "~/models/note";
import styles from "./NoteDetails.css";

interface NoteDetailsProps extends Note {}

const NoteDetails: FC<NoteDetailsProps> = (props) => {
  return (
    <section id='note-details'>
      <header>
        <nav>
          <Link to={"/notes"}>Back to all Notes</Link>
        </nav>
        <h1>{props.title}</h1>
      </header>

      <div>
        <p id='note-detials-content'>{props.content}</p>
      </div>
    </section>
  );
};

export default NoteDetails;

export const noteDetailsLinkDescriptors: LinkDescriptor[] = [
  { rel: "stylesheet", href: styles },
];
