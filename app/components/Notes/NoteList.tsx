import type { LinkDescriptor } from "@remix-run/node";
import type { FC } from "react";
import type { Note } from "~/models/note";
import { NoteItem } from "./NoteItem";
import styles from "./NoteList.css";

interface NoteListProps {
  notes: Note[];
}

const NoteList: FC<NoteListProps> = ({ notes }) => {
  return (
    <ul id='note-list'>
      {notes.map((note, index) => (
        <NoteItem
          key={note.id}
          id={note.id}
          title={note.title}
          content={note.content}
          index={index}
        />
      ))}
    </ul>
  );
};

export default NoteList;

export const noteListlinksDescriptor: LinkDescriptor[] = [
  { rel: "stylesheet", href: styles },
];
