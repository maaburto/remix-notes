import type { LinkDescriptor } from "@remix-run/node";
import type { FC } from "react";
import styles from "./NoteItemMeta.css";

interface NoteItemMetaProps {
  id: string;
  index: number;
}

const NoteItemMeta: FC<NoteItemMetaProps> = (props) => {
  return (
    <ul className='note-meta'>
      <li>#{props.index + 1}</li>
      <li>
        <time dateTime={props.id}>
          {new Date(props.id).toLocaleDateString("en-US", {
            day: "numeric",
            month: "short",
            year: "numeric",
            hour: "2-digit",
            minute: "2-digit",
          })}
        </time>
      </li>
    </ul>
  );
};

export default NoteItemMeta;

export const noteItemMetalinksDescriptor: LinkDescriptor[] = [
  { rel: "stylesheet", href: styles },
];
