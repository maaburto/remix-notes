import type { LinkDescriptor } from "@remix-run/node";
import { Link } from "@remix-run/react";
import type { FC } from "react";
import type { Note } from "~/models/note";
import { NoteItemMeta } from "../NoteItemMeta";
import styles from "./NoteItem.css";

interface NoteItemProps extends Note {
  index: number;
}

const NoteItem: FC<NoteItemProps> = (props) => {
  return (
    <li className='note'>
      <Link to={props.id}>
        <article>
          <header>
            <NoteItemMeta id={props.id} index={props.index} />
            <h2>{props.title}</h2>
          </header>
          <p>{props.content}</p>
        </article>
      </Link>
    </li>
  );
};

export default NoteItem;

export const noteItemlinksDescriptor: LinkDescriptor[] = [
  { rel: "stylesheet", href: styles },
];
